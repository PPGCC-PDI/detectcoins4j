package utfpr.ppgcc.pdi.detectMoney.stats.impl;

import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import utfpr.ppgcc.pdi.detectMoney.ImageTab;
import utfpr.ppgcc.pdi.detectMoney.ImageTab.ImageData;
import utfpr.ppgcc.pdi.detectMoney.seg.impl.BinaryNeighborhoodSegmentation;
import utfpr.ppgcc.pdi.detectMoney.stats.Statistic;

public class MoneyCounterStatistic implements Statistic {

	private final ExecutorService exec = Executors.newCachedThreadPool();

	@Override
	public String getLabel() {
		return "Contagem de Dinheiro";
	}

	@Override
	public void show(ImageTab tab) {
		ImageData data = tab.getCurrentImage();

		exec.submit(() -> showResults(apply(data)));
	}

	private void showResults(Map<Coin, Long> result) {
		Platform.runLater(() -> {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle(this.getLabel());
			alert.setContentText(parseResults(result));
			alert.show();
		});
	}

	public static String parseResults(Map<Coin, Long> result) {
		String details = result.keySet().stream().map(c -> {
			double unValue = c.getValue();
			int amount = result.get(c).intValue();
			double total = unValue * amount;
			return String.format("R$ %.2f x %d= R$ %.2f\n", unValue, amount, total);
		}).collect(Collectors.joining());
		String total = String.format("Valor Total: %.2f",
				result.keySet().stream().mapToDouble(c -> c.getValue() * result.get(c)).sum());
		return details + total;
	}

	public static Map<Coin, Long> apply(ImageData data) {
		BinaryNeighborhoodSegmentation seg = new BinaryNeighborhoodSegmentation();
		return seg.apply(data).stream().mapToInt(d -> BlackPixelCountStatistic.count(d.getImage()))
				.mapToObj(Coin::getByPixelCount)
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
	}

	static enum Coin {
		COIN_100(1.0, 79300), COIN_050(0.5, 58000), COIN_025(0.25, 68000), COIN_010(0.1, 43000);

		private double value;
		private int avgPixelsCount;

		Coin(double value, int avgPixelsCount) {
			this.value = value;
			this.avgPixelsCount = avgPixelsCount;
		}

		public double getValue() {
			return value;
		}

		public int getAvgPixelsCount() {
			return avgPixelsCount;
		}

		public static Coin getByPixelCount(int count) {
			return Stream.of(Coin.values()).map(c -> new CoinSubtractResult(c, c.getAvgPixelsCount() - count)).sorted()
					.findFirst().map(CoinSubtractResult::getCoin).orElse(null);
		}
	}

	static class CoinSubtractResult implements Comparable<CoinSubtractResult> {
		private Coin coin;
		private int result;

		public CoinSubtractResult(Coin coin, int result) {
			this.coin = coin;
			this.result = result;
		}

		public Coin getCoin() {
			return coin;
		}

		public int getResultUnsigned() {
			return result > 0 ? result : -result;
		}

		@Override
		public int compareTo(CoinSubtractResult o) {
			return this.getResultUnsigned() - o.getResultUnsigned();
		}
	}
}
