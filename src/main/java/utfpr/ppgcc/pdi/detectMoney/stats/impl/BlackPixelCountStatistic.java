package utfpr.ppgcc.pdi.detectMoney.stats.impl;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;
import utfpr.ppgcc.pdi.detectMoney.ImageTab;
import utfpr.ppgcc.pdi.detectMoney.stats.Statistic;

public class BlackPixelCountStatistic implements Statistic {

	@Override
	public String getLabel() {
		return "Contagem de Pixels Pretos";
	}

	@Override
	public void show(ImageTab tab) {

		int total = count(tab.getCurrentImage().getImage());
		Platform.runLater(() -> {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle(this.getLabel());
			alert.setContentText(String.format("Total de pixels pretos: %d", total));
			alert.show();
		});
	}

	public static int count(Image image) {
		double w = image.getWidth();
		double h = image.getHeight();
		PixelReader reader = image.getPixelReader();
		int count = 0;
		for (int i = 0; i < w; i++) {
			for (int j = 0; j < h; j++) {
				int x = i;
				int y = j;
				Color color = reader.getColor(x, y);
				if (color.getRed() < 0.5) {
					count++;
				}
			}
		}
		return count;
	}

}
