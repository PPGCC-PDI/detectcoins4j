package utfpr.ppgcc.pdi.detectMoney.stats;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;

import utfpr.ppgcc.pdi.detectMoney.ImageTab;
import utfpr.ppgcc.pdi.detectMoney.stats.impl.BlackPixelCountStatistic;
import utfpr.ppgcc.pdi.detectMoney.stats.impl.MoneyCounterStatistic;

public interface Statistic {
	String getLabel();

	default void showAsync(ImageTab tab) {
		Executors.newSingleThreadExecutor().execute(() -> this.show(tab));
	}

	void show(ImageTab tab);

	static List<Statistic> all() {
		return Arrays.asList(new BlackPixelCountStatistic(), new MoneyCounterStatistic());
	}
}
