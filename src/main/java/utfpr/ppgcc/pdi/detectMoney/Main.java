package utfpr.ppgcc.pdi.detectMoney;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) throws Exception {
		Parent content = FXMLLoader.load(Main.class.getResource(MainWindow.FXML));
		primaryStage.setScene(new Scene(content));
		primaryStage.setMaximized(true);
		primaryStage.show();
		primaryStage.setOnCloseRequest(e->System.exit(0));
	}

	public static void main(String[] args) {
		Application.launch(args);
	}

}
