package utfpr.ppgcc.pdi.detectMoney;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.function.Supplier;

import javax.imageio.ImageIO;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import utfpr.ppgcc.pdi.detectMoney.seg.Segmentation;
import utfpr.ppgcc.pdi.detectMoney.transf.Transformation;

public class ImageTab {
	public static final String FXML = "/" + ImageTab.class.getCanonicalName().replace(".", "/") + ".fxml";

	private static final ExecutorService EXECUTOR = Executors.newCachedThreadPool();

	private static final String EMPTY_FILE = "Nenhum...";

	private final ObjectProperty<ImageData> images = new SimpleObjectProperty<>();

	@FXML
	private Tab tab;

	@FXML
	private BorderPane root;

	@FXML
	private ScrollPane content;

	@FXML
	public void initialize() {
		this.tab.setText(EMPTY_FILE);
		this.images.addListener(this::onImagesChange);
	}

	public boolean isControllerOf(Tab tab) {
		return this.tab.equals(tab);
	}

	public void set(File file) {
		EXECUTOR.submit(() -> {
			try {
				ImageData data = new ImageData(file);
				Platform.runLater(() -> {
					this.images.set(data);
				});

			} catch (IOException e) {
				this.showError(e);
			}
		});
	}

	public void onImagesChange(ObservableValue<? extends ImageData> observable, ImageData oldValue,
			ImageData newValue) {
		tab.setText(Optional.ofNullable(newValue).map(ImageData::getName).orElse(EMPTY_FILE));
		this.drawImages();
	}

	private void drawImages() {
		Platform.runLater(() -> {
			AnchorPane pane = new AnchorPane();
			Optional.ofNullable(this.images.get()).map(ImageData::getImage).map(img -> {
				ImageView view = new ImageView();
				view.setImage(img);
				return view;
			}).ifPresent(pane.getChildren()::add);
			setImageContent(pane);
		});
	}

	private void setImageContent(AnchorPane pane) {
		content.setContent(pane);
	}

	private void showError(IOException e) {
		e.printStackTrace();
	}

	public void apply(Transformation t) {
		EXECUTOR.submit(() -> {
			Optional.ofNullable(this.images.get()).ifPresent(i -> i.apply(t));
			this.drawImages();
		});
	}

	public void applyOnNew(Transformation t, ImageTab tab) {
		EXECUTOR.submit(() -> {
			Optional.ofNullable(this.images.get()).map(i -> i.applyAsNew(t)).ifPresent(data -> Platform.runLater(() -> {
				tab.images.set(data);
			}));
		});
	}

	public void applyOnNew(Segmentation seg, Supplier<ImageTab> sup) {
		EXECUTOR.submit(() -> {
			List<ImageData> apply = seg.apply(this.images.get());
			Platform.runLater(() -> {
				apply.stream().forEach(data -> sup.get().images.set(data));
			});
		});
	}

	public void saveTo(File result) {
		this.getCurrentImage().saveTo(result);
	}

	public ImageData getCurrentImage() {
		return this.images.get();
	}

	public static class ImageData {
		private String id = UUID.randomUUID().toString();

		private String name;

		private Image image;

		public ImageData(String name, Image image) {
			this.name = name;
			this.image = image;
		}

		public ImageData(File file) throws IOException {
			this.name = file.getName();
			this.image = new Image(file.toURI().toURL().toString(), true);
		}

		public void saveTo(File result) {
			BufferedImage bufferedImage = SwingFXUtils.fromFXImage(this.image, null);
			try {
				ImageIO.write(bufferedImage, "png", result);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}

		public String getId() {
			return id;
		}

		public String getName() {
			return name;
		}

		public Image getImage() {
			return image;
		}

		public void update(Image image) {
			this.image = image;
		}

		public void apply(Transformation t) {
			this.image = t.apply(image);
		}

		public ImageData applyAsNew(Transformation t) {
			return new ImageData(this.name, t.apply(image));
		}
	}

	public static ImageTab create(Consumer<Tab> finishAction) {
		try {
			FXMLLoader loader = new FXMLLoader(ImageTab.class.getResource(FXML));
			finishAction.accept(loader.load());
			ImageTab controller = loader.getController();
			return controller;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
