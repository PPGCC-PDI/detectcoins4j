package utfpr.ppgcc.pdi.detectMoney;

public class MaskConfigurations {

	private static final MaskConfigurations INSTANCE = new MaskConfigurations();

	private int defaultSize = 9;

	private MaskConfigurations() {
	}

	public int getDefaultSize() {
		return defaultSize;
	}

	public static MaskConfigurations instance() {
		return INSTANCE;
	}
}
