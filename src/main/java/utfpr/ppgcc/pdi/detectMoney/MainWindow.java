package utfpr.ppgcc.pdi.detectMoney;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import utfpr.ppgcc.pdi.detectMoney.ImageTab.ImageData;
import utfpr.ppgcc.pdi.detectMoney.seg.Segmentation;
import utfpr.ppgcc.pdi.detectMoney.stats.Statistic;
import utfpr.ppgcc.pdi.detectMoney.stats.impl.MoneyCounterStatistic;
import utfpr.ppgcc.pdi.detectMoney.transf.Transformation;
import utfpr.ppgcc.pdi.detectMoney.transf.impl.DilatationTransformation;
import utfpr.ppgcc.pdi.detectMoney.transf.impl.ErosionTransformation;
import utfpr.ppgcc.pdi.detectMoney.transf.impl.ToBinaryTransformation;

public class MainWindow {
	public static final String FXML = "/" + MainWindow.class.getCanonicalName().replace(".", "/") + ".fxml";

	@FXML
	private BorderPane root;

	@FXML
	private ToolBar toolbar;

	@FXML
	private Button openFile;

	@FXML
	private Button saveFile;

	@FXML
	private MenuButton transformations;

	@FXML
	private MenuButton segmentations;

	@FXML
	private MenuButton statistics;

	@FXML
	private Button filterAndCount;

	@FXML
	private TabPane tabView;

	private BooleanProperty loading = new SimpleBooleanProperty(false);

	private BooleanProperty actionsEnabled = new SimpleBooleanProperty(false);

	private final List<ImageTab> tabsCtrls = new ArrayList<>();

	@FXML
	public void initialize() {
		tabView.getTabs().addListener(this::onTabsChange);
		this.actionsEnabled.bind(this.loading.not());

		Transformation.all().stream().map(i -> {
			MenuItem item = new MenuItem(i.getLabel());
			item.setOnAction(e -> Platform.runLater(() -> this.getCurrentTab().ifPresent(t -> {
				ImageTab tab = ImageTab.create(tabView.getTabs()::add);
				t.applyOnNew(i, tab);
				tabsCtrls.add(tab);
			})));
			return item;
		}).forEachOrdered(transformations.getItems()::add);

		Statistic.all().stream().map(i -> {
			MenuItem item = new MenuItem(i.getLabel());
			item.setOnAction(e -> this.getCurrentTab().ifPresent(t -> i.showAsync(t)));
			return item;
		}).forEachOrdered(statistics.getItems()::add);

		Segmentation.all().stream().map(i -> {
			MenuItem item = new MenuItem(i.getLabel());
			item.setOnAction(e -> this.getCurrentTab().ifPresent(t -> t.applyOnNew(i, () -> {
				ImageTab tab = ImageTab.create(tabView.getTabs()::add);
				tabsCtrls.add(tab);
				return tab;
			})));
			return item;
		}).forEachOrdered(segmentations.getItems()::add);

	}

	@FXML
	public void applyFiltersAndCount() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Processo de Contagem");
		alert.setContentText("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		AtomicInteger step = new AtomicInteger(0);
		long start = System.currentTimeMillis();
		AtomicLong last = new AtomicLong(System.currentTimeMillis());
		StringBuilder log = new StringBuilder();
		Consumer<String> showMessage = message -> {
			long now = System.currentTimeMillis();
			String templatedMessage = String.format(" [%,d ms]\n%d - %s", (now - last.get()), step.incrementAndGet(),
					message);
			last.set(now);
			this.showMessage(templatedMessage);
			log.append(templatedMessage);
			Platform.runLater(() -> {
				alert.headerTextProperty().set(message);
				alert.contentTextProperty().set(log.toString());
			});
		};
		Consumer<String> showFinishMessage = message -> {
			long now = System.currentTimeMillis();
			String templatedMessage = String.format(" [%,d ms]\nFinalizado em [%,d ms]", (now - last.get()), (now - start));
			this.showMessage(templatedMessage);
			log.append(templatedMessage);
			this.showMessage(message);
			log.append("\n").append(message);
			Platform.runLater(() -> {
				alert.headerTextProperty().set("Finalizado");
				alert.contentTextProperty().set(log.toString());
			});
		};
		showMessage.accept("Inciando");
		alert.show();
		Executors.newSingleThreadExecutor().submit(() -> {
			Platform.runLater(() -> this.loading.set(true));
			this.getCurrentTab().ifPresent(t -> {
				ImageData image = t.getCurrentImage();
				showMessage.accept("Binarizando");
				image = image.applyAsNew(new ToBinaryTransformation());
				showMessage.accept("Aplicando Dilatação");
				image = image.applyAsNew(new DilatationTransformation());
				showMessage.accept("Aplicando Erosão");
				image = image.applyAsNew(new ErosionTransformation());
				showMessage.accept("Contagem de Moedas");
				showFinishMessage.accept(MoneyCounterStatistic.parseResults(MoneyCounterStatistic.apply(image)));
			});
			Platform.runLater(() -> this.loading.set(false));
		});
	}

	private void showMessage(String string) {
		System.out.println(string);
	}

	@FXML
	public void showFileInput() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Selecione uma imagem");
		List<File> list = fileChooser.showOpenMultipleDialog(root.getScene().getWindow());
		if (list != null && !list.isEmpty()) {
			list.stream().map(f -> {
				ImageTab t = ImageTab.create(tabView.getTabs()::add);
				t.set(f);
				return t;
			}).forEachOrdered(tabsCtrls::add);
		}
	}

	@FXML
	public void showFileOutput() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Selecione um destino");
		File result = fileChooser.showSaveDialog(root.getScene().getWindow());
		if (result != null) {
			this.getCurrentTab().ifPresent(t -> t.saveTo(result));
		}
	}

	public Optional<ImageTab> getCurrentTab() {
		int idx = this.tabView.getSelectionModel().getSelectedIndex();
		if (idx >= 0) {
			return Optional.of(tabsCtrls.get(idx));
		} else {
			return Optional.empty();
		}
	}

	private void onTabsChange(ListChangeListener.Change<? extends Tab> change) {
		if (change.next()) {
			if (change.wasRemoved())
				change.getRemoved().forEach(t -> this.tabsCtrls.removeIf(ctrl -> ctrl.isControllerOf(t)));
			if (change.wasAdded())
				this.tabView.getSelectionModel().selectLast();
		}
	}

	public BooleanProperty loadingProperty() {
		return loading;
	}

	public BooleanProperty actionsEnabledProperty() {
		return actionsEnabled;
	}
}
