package utfpr.ppgcc.pdi.detectMoney.transf.impl;

import utfpr.ppgcc.pdi.detectMoney.transf.CombinedTransformation;

public class BorderSmoothingTransformation extends CombinedTransformation {

	public BorderSmoothingTransformation() {
		super(new DilatationTransformation(), new ErosionTransformation());
	}

	@Override
	public String getLabel() {
		return "Suavisação de Bordas";
	}

}
