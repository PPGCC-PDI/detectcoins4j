package utfpr.ppgcc.pdi.detectMoney.transf;

import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;
import utfpr.ppgcc.pdi.detectMoney.transf.PixelTransformation;

public class SubtractTransformation implements PixelTransformation {

	@Override
	public String getLabel() {
		return "Escala de cinza";
	}

	public Color transform(PixelReader in, int w, int h) {
		return in.getColor(w, h).grayscale();
	}

}
