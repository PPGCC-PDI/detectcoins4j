package utfpr.ppgcc.pdi.detectMoney.transf;

import java.util.function.Predicate;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;
import utfpr.ppgcc.pdi.detectMoney.MaskConfigurations;

public class MaskIterator {

	private int maskSize;
	private int borderSizeX;
	private int borderSizeY;

	public MaskIterator() {
		this(MaskConfigurations.instance().getDefaultSize());
	}

	public MaskIterator(int size) {
		this(size, size);
	}

	public MaskIterator(int maskHeigth, int maskWidth) {
		this.maskSize = (maskWidth) * (maskHeigth);
		this.borderSizeX = (maskWidth - 1) / 2;
		this.borderSizeY = (maskHeigth - 1) / 2;
	}

	public Stream<Pixel> generateStream(PixelReader reader, int x, int y) {

		return IntStream.range(0, maskSize).mapToObj(i -> {
			try {
				int posX = (i / ((borderSizeX * 2) + 1)) - borderSizeX + x;
				int posY = (i % ((borderSizeX * 2) + 1)) - borderSizeY + y;
				Color color = reader.getColor(posX, posY);
				return new Pixel(posX, posY, color);
			} catch (ArithmeticException | IndexOutOfBoundsException e) {
				return null;
			}
		}).distinct().filter(Predicate.isEqual(null).negate());
	}

	public class Pixel implements Comparable<Pixel> {
		private final int x;
		private final int y;
		private final Color color;

		private Pixel(int x, int y, Color color) {
			this.x = x;
			this.y = y;
			this.color = color;
		}

		public int getX() {
			return x;
		}

		public int getY() {
			return y;
		}

		public Color getColor() {
			return color;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof Pixel) {
				Pixel p = (Pixel) obj;
				return this.x == p.x && p.y == this.y;
			}
			return super.equals(obj);
		}

		@Override
		public int compareTo(Pixel o) {
			int lineDiff = this.y - o.y;
			if (lineDiff != 0) {
				return lineDiff;
			} else {
				return this.x - o.x;
			}
		}
	}

}
