package utfpr.ppgcc.pdi.detectMoney.transf.impl;

import java.util.function.ToDoubleFunction;

import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;
import utfpr.ppgcc.pdi.detectMoney.transf.ColorOperator;
import utfpr.ppgcc.pdi.detectMoney.transf.MaskIterator;
import utfpr.ppgcc.pdi.detectMoney.transf.PixelTransformation;
import utfpr.ppgcc.pdi.detectMoney.transf.MaskIterator.Pixel;

public class DilatationTransformation implements PixelTransformation {
	@Override
	public String getLabel() {
		return "Dilatação";
	}

	public Color transform(PixelReader in, int w, int h) {
		MaskIterator maskIterator = new MaskIterator();

		ToDoubleFunction<ToDoubleFunction<Color>> streamGen = channelExtractor -> maskIterator.generateStream(in, w, h)
				.map(Pixel::getColor).mapToDouble(channelExtractor).min().orElse(0.0);

		return ColorOperator.apply(in.getColor(w, h), streamGen);
	}

}
