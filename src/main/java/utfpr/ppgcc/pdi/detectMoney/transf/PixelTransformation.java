package utfpr.ppgcc.pdi.detectMoney.transf;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public interface PixelTransformation extends Transformation {

	Color transform(PixelReader in, int w, int h);

	default Image apply(Image image) {
		double w = image.getWidth();
		double h = image.getHeight();
		PixelReader reader = image.getPixelReader();
		WritableImage result = new WritableImage((int) w, (int) h);
		ExecutorService threadPool = Executors.newCachedThreadPool();
		for (int i = 0; i < w; i++) {
			for (int j = 0; j < h; j++) {
				int x = i;
				int y = j;
				threadPool.submit(() -> result.getPixelWriter().setColor(x, y, this.transform(reader, x, y)));
			}
		}
		return result;
	}
}
