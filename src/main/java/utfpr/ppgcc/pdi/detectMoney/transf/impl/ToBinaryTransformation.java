package utfpr.ppgcc.pdi.detectMoney.transf.impl;

import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;
import utfpr.ppgcc.pdi.detectMoney.transf.PixelTransformation;

public class ToBinaryTransformation implements PixelTransformation {

	@Override
	public String getLabel() {
		return "Binarização";
	}

	public Color transform(PixelReader in, int w, int h) {
		Color grayscale = in.getColor(w, h).grayscale();
		return grayscale.getRed() > 0.5 ? Color.gray(1) : Color.gray(0);
	}

}
