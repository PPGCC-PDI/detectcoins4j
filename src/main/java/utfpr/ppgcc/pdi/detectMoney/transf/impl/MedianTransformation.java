package utfpr.ppgcc.pdi.detectMoney.transf.impl;

import java.util.function.ToDoubleFunction;

import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;
import utfpr.ppgcc.pdi.detectMoney.transf.ColorOperator;
import utfpr.ppgcc.pdi.detectMoney.transf.MaskIterator;
import utfpr.ppgcc.pdi.detectMoney.transf.MaskIterator.Pixel;
import utfpr.ppgcc.pdi.detectMoney.transf.PixelTransformation;

public class MedianTransformation implements PixelTransformation {

	@Override
	public String getLabel() {
		return "Mediana";
	}

	public Color transform(PixelReader in, int w, int h) {
		MaskIterator maskIterator = new MaskIterator();

		ToDoubleFunction<ToDoubleFunction<Color>> streamGen = channelExtractor -> this.median(maskIterator
				.generateStream(in, w, h).map(Pixel::getColor).mapToDouble(channelExtractor).sorted().toArray());

		return ColorOperator.apply(in.getColor(w, h), streamGen);
	}

	private double median(double[] apply) {
		if (apply.length == 0) {
			return 0.0;
		}
		if (apply.length % 2 != 0) {
			return apply[apply.length / 2];
		} else {
			return (apply[apply.length / 2 - 1] + apply[(apply.length) / 2]) / 2.0;
		}
	}

}
