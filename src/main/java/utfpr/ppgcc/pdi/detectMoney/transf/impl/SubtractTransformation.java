package utfpr.ppgcc.pdi.detectMoney.transf.impl;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;
import utfpr.ppgcc.pdi.detectMoney.transf.ColorOperator;
import utfpr.ppgcc.pdi.detectMoney.transf.PixelTransformation;

public class SubtractTransformation implements PixelTransformation {

	private Image image;
	private PixelReader reader;

	public SubtractTransformation(Image image) {
		this.image = image;
		reader = image.getPixelReader();
	}

	@Override
	public String getLabel() {
		return "Subtração";
	}

	@Override
	public Image apply(Image image) {
		if (image.getWidth() != this.image.getWidth() || image.getHeight() != this.image.getHeight()) {
			throw new IllegalArgumentException();
		}
		return PixelTransformation.super.apply(image);
	}

	public Color transform(PixelReader in, int w, int h) {
		Color color = in.getColor(w, h);
		return ColorOperator.apply(color, channelExtractor -> {
			double channel = channelExtractor.applyAsDouble(color);
			double otherChannel = channelExtractor.applyAsDouble(this.reader.getColor(w, h));
			double result = channel - otherChannel;
			if (result < 0) {
				return 0D;
			} else if (result > 1) {
				return 1D;
			} else {
				return result;
			}
		});
	}

}
