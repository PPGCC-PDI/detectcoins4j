package utfpr.ppgcc.pdi.detectMoney.transf;

import java.util.Arrays;
import java.util.List;

import javafx.scene.image.Image;

public abstract class CombinedTransformation implements Transformation {
	private final List<Transformation> steps;

	public CombinedTransformation(Transformation... steps) {
		this(Arrays.asList(steps));
	}

	public CombinedTransformation(List<Transformation> steps) {
		this.steps = steps;
	}

	public Image apply(Image image) {
		Image result = image;
		for (Transformation t : steps) {
			result = t.apply(result);
		}
		return result;
	}

}
