package utfpr.ppgcc.pdi.detectMoney.transf.impl;

import javafx.scene.image.Image;
import utfpr.ppgcc.pdi.detectMoney.transf.CombinedTransformation;

public class BorderTransformation extends CombinedTransformation {

	public BorderTransformation() {
		super(new DilatationTransformation());
	}

	@Override
	public String getLabel() {
		return "Bordas";
	}

	@Override
	public Image apply(Image image) {
		Image result = super.apply(image);
		return new SubtractTransformation(image).apply(result);
	}
}
