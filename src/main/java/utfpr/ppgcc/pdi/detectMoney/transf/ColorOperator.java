package utfpr.ppgcc.pdi.detectMoney.transf;

import java.util.function.ToDoubleFunction;

import javafx.scene.paint.Color;

public class ColorOperator {
	public static boolean isBlack(Color color) {
		return color.getBlue() == 0.0 && color.getGreen() == 0.0 && color.getRed() == 0.0;
	}

	public static boolean isBinaryBlack(Color color) {
		return color.grayscale().getRed() < 0.5;
	}

	public static boolean isWhite(Color color) {
		return color.getBlue() == 1.0 && color.getGreen() == 1.0 && color.getRed() == 1.0;
	}

	public static boolean isBinaryWhite(Color color) {
		return !isBinaryBlack(color);
	}

	public static boolean isGrayscale(Color color) {
		return color.getBlue() == color.getRed() && color.getBlue() == color.getGreen();
	}

	public static Color apply(Color color, ToDoubleFunction<ToDoubleFunction<Color>> operator) {
		return isGrayscale(color) ? applyGrayscale(operator, color.getOpacity())
				: applyAllChannels(operator, color.getOpacity());
	}

	public static Color applyGrayscale(ToDoubleFunction<ToDoubleFunction<Color>> operator, double opacity) {
		double c = operator.applyAsDouble(Color::getRed);
		return Color.gray(c, opacity);
	}

	public static Color applyAllChannels(ToDoubleFunction<ToDoubleFunction<Color>> operator, double opacity) {
		double red = operator.applyAsDouble(Color::getRed);
		double green = operator.applyAsDouble(Color::getGreen);
		double blue = operator.applyAsDouble(Color::getBlue);
		return new Color(red, green, blue, opacity);
	}

}
