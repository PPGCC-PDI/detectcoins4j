package utfpr.ppgcc.pdi.detectMoney.transf.impl;

import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;
import utfpr.ppgcc.pdi.detectMoney.transf.PixelTransformation;

public class InverseTransformation implements PixelTransformation {

	@Override
	public String getLabel() {
		return "Inversão";
	}

	public Color transform(PixelReader in, int w, int h) {
		return in.getColor(w, h).invert();
	}

}
