package utfpr.ppgcc.pdi.detectMoney.transf;

import java.util.Arrays;
import java.util.List;

import javafx.scene.image.Image;
import utfpr.ppgcc.pdi.detectMoney.transf.impl.BorderSmoothingTransformation;
import utfpr.ppgcc.pdi.detectMoney.transf.impl.BorderTransformation;
import utfpr.ppgcc.pdi.detectMoney.transf.impl.DilatationTransformation;
import utfpr.ppgcc.pdi.detectMoney.transf.impl.ErosionTransformation;
import utfpr.ppgcc.pdi.detectMoney.transf.impl.GrayscaleTransformation;
import utfpr.ppgcc.pdi.detectMoney.transf.impl.InverseTransformation;
import utfpr.ppgcc.pdi.detectMoney.transf.impl.MedianTransformation;
import utfpr.ppgcc.pdi.detectMoney.transf.impl.ToBinaryTransformation;
import utfpr.ppgcc.pdi.detectMoney.transf.impl.ZoomOutTransformation;

public interface Transformation {
	String getLabel();

	Image apply(Image image);

	static List<Transformation> all() {
		return Arrays.asList(new ZoomOutTransformation(), new GrayscaleTransformation(), new MedianTransformation(),
				new ToBinaryTransformation(), new InverseTransformation(), new DilatationTransformation(),
				new ErosionTransformation(), new BorderTransformation(), new BorderSmoothingTransformation());
	}
}
