package utfpr.ppgcc.pdi.detectMoney.transf;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.DoubleStream;

public class Mask implements Iterable<Double> {

	private final double[] mask;

	private final int width;
	private final int height;

	public Mask(double[] mask) {
		this.mask = mask;
		this.height = this.width = (int) (mask != null ? Math.sqrt(mask.length) : 0);
		if ((this.height * this.width) != mask.length) {
			throw new IllegalArgumentException();
		}
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	public double[] getMask() {
		return mask;
	}

	@Override
	public Iterator<Double> iterator() {

		return new Iterator<Double>() {
			private AtomicInteger cursor = new AtomicInteger(0);

			@Override
			public boolean hasNext() {
				return cursor.get() < mask.length;
			}

			@Override
			public Double next() {
				return mask[cursor.getAndIncrement()];
			}
		};
	}

	public DoubleStream stream() {
		return DoubleStream.of(mask);
	}

}
