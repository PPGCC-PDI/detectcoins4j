package utfpr.ppgcc.pdi.detectMoney.transf.impl;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import utfpr.ppgcc.pdi.detectMoney.transf.Transformation;

public class ZoomOutTransformation implements Transformation {

	@Override
	public String getLabel() {
		return "Zoom Out";
	}

	@Override
	public Image apply(Image image) {
		int w = half(image.getWidth());
		int h = half(image.getHeight());
		WritableImage result = new WritableImage(w, h);
		PixelReader in = image.getPixelReader();
		PixelWriter out = result.getPixelWriter();
		for (int wi = 0; wi < w; wi++) {
			for (int hi = 0; hi < h; hi++) {
				int pixel = in.getArgb(wi * 2, hi * 2);
				out.setArgb(wi, hi, pixel);
			}
		}
		return result;
	}

	private int half(double d) {
		return (int) (d / 2);
	}

}
