package utfpr.ppgcc.pdi.detectMoney.seg.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;
import utfpr.ppgcc.pdi.detectMoney.ImageTab.ImageData;
import utfpr.ppgcc.pdi.detectMoney.seg.Rect;
import utfpr.ppgcc.pdi.detectMoney.seg.Segmentation;
import utfpr.ppgcc.pdi.detectMoney.transf.ColorOperator;
import utfpr.ppgcc.pdi.detectMoney.transf.MaskIterator;

public class BinaryNeighborhoodSegmentation implements Segmentation {

	@Override
	public String getLabel() {
		return "Segmentação por Vizinhança Binária";
	}

	@Override
	public List<ImageData> apply(ImageData data) {

		Image image = data.getImage();
		List<Rect> objects = selectObjects(image);

		return objects.stream().map(r -> {
			Image segment = r.segment(image);
			return new ImageData(data.getName() + r, segment);
		}).collect(Collectors.toList());
	}

	private List<Rect> selectObjects(Image image) {
		List<Rect> result = new ArrayList<>();
		double width = image.getWidth();
		double height = image.getHeight();
		PixelReader reader = image.getPixelReader();
		MaskIterator mask = new MaskIterator(15);
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				int fx = x;
				int fy = y;
				Color color = reader.getColor(x, y);
				if (ColorOperator.isBinaryBlack(color)) {
					Optional<Rect> curr = mask.generateStream(reader, x, y)
							.flatMap(p -> result.stream().filter(r -> r.contains(p.getX(), p.getY()))).findAny();
					if (curr.isPresent()) {
						curr.get().add(fx, fy);
					} else {
						result.add(new Rect(fx, fy));
					}
				}
			}
		}
		result.removeIf(r -> r.getHeigth() < 100 && r.getWidth() < 100);
		System.out.println("Selecionados:");
		result.forEach(System.out::println);
		return result;
	}

}
