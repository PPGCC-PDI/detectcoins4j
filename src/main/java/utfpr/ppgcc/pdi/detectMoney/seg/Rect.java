package utfpr.ppgcc.pdi.detectMoney.seg;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;

public class Rect {

	private int minX = 0;
	private int maxX = 0;
	private int minY = 0;
	private int maxY = 0;

	public Rect(int x, int y) {
		this.minX = this.maxX = x;
		this.minY = this.maxY = y;
	}

	public void add(int x, int y) {
		if (x < minX)
			this.minX = x;
		if (x > maxX)
			this.maxX = x;
		if (y < minY)
			this.minY = y;
		if (y > maxY)
			this.maxY = y;
	}

	public boolean contains(int x, int y) {
		return x <= maxX && x >= minX && y <= maxY && y >= minY;
	}

	public int getWidth() {
		return this.maxX - this.minX;
	}

	public int getHeigth() {
		return this.maxY - this.minY;
	}

	public Image segment(Image src) {
		WritableImage image = new WritableImage(getWidth(), getHeigth());
		PixelReader in = src.getPixelReader();
		PixelWriter writer = image.getPixelWriter();
		for (int x = minX; x < maxX; x++) {
			for (int y = minY; y < maxY; y++) {
				writer.setArgb(x - minX, y - minY, in.getArgb(x, y));
			}
		}
		return image;
	}

	@Override
	public String toString() {
		return String.format("(%d,%d)-(%d,%d)", this.minX, this.minY, this.maxX, this.maxY);
	}
}