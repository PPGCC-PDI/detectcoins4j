package utfpr.ppgcc.pdi.detectMoney.seg;

import java.util.Arrays;
import java.util.List;

import utfpr.ppgcc.pdi.detectMoney.ImageTab.ImageData;
import utfpr.ppgcc.pdi.detectMoney.seg.impl.BinaryNeighborhoodSegmentation;

public interface Segmentation {

	String getLabel();

	List<ImageData> apply(ImageData data);

	static List<Segmentation> all() {
		return Arrays.asList(new BinaryNeighborhoodSegmentation());
	}

}
